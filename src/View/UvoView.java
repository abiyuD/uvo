package View;

import Controller.UvoFungsi;
import Model.UvoModel;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author abiyu
 */
public class UvoView {

    static Scanner input = new Scanner(System.in);
    static Scanner inputInt = new Scanner(System.in);
    static UvoModel um;
    static UvoFungsi uf;
    static Statement stm;
    static String id;

    static DecimalFormat df = new DecimalFormat("#,###,##0.00");
    static DecimalFormat df1 = new DecimalFormat("#,###,##0");

    public static void koneksi() throws SQLException {
        try {
            String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=uvo;"
                    + "user=sa;"
                    + "password=12345";
            Connection conn = DriverManager.getConnection(connectionUrl);
            stm = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void login() throws SQLException {
        String username, user = null, pass = null, password;
        koneksi();

        System.out.println("--------Menu UVO--------");
        System.out.println("========================");
        System.out.print("Masukkan id anda: ");
        username = input.next();

        System.out.print("Masukkan password anda: ");
        password = input.next();

        String SQL = "Select * from users where id_user='" + username + "'";
        ResultSet rs = stm.executeQuery(SQL);

        // menggambil saldo dari model
        while (rs.next()) {
            user = rs.getString("id_user");
            pass = rs.getString("password");
        }

        // cek username dan sandi
        if (username.equals(user) && password.equals(pass)) {
            // manggil menu dari controller
            id = user;
            uf.menu();

        } else {
            System.out.println("Username atau Password salah");

            // manggil login dari controller
            uf.login();
        }
    }

    public void menu() throws SQLException {
        boolean benar = true;
        String pilih;
        System.out.println("\nMenu Transaksi Anda Silahkan Pilih Option");
        System.out.println("1. Lihat Saldo UVO");
        System.out.println("2. Top Up UVO");
        System.out.println("3. PLN");
        System.out.println("4. Pulsa");
        System.out.println("5. UVO Point");
        System.out.println("6. Keluar");

        do {
            System.out.print("Masukkan pilihan anda: ");
            pilih = input.next();

            switch (pilih) {
                case "1":
                    benar = false;
                    // manggil view lihat saldo dari controller
                    uf.lihatSaldo();
                    break;

                case "2":
                    benar = false;
                    // manggil view Top Up dari controller
                    uf.topup();
                    break;
                case "3":
                    benar = false;
                    // manggil view pln dari controller
                    uf.bayarPln();
                    break;

                case "4":
                    benar = false;
                    // manggil view pulsa dari controller
                    uf.pulsa();
                    break;

                case "5":
                    benar = false;
                    // manggil view lihatUvoPoint dari controller
                    uf.uvoPoint();
                    break;

                case "6":
                    benar = false;
                    // manggil view keluar dari controller
                    uf.keluar();
                    break;

                default:
                    System.out.println("Maaf pilihan tidak terdaftar, harap masukkan 1-6");
                    break;
            }
        } while (benar);

    }

    public static void lihatSaldo() throws SQLException {
        koneksi();
        int pilih;
        boolean benar = true;
        String SQL = "Select * from users where id_user='" + id + "'";
        ResultSet rs = stm.executeQuery(SQL);
        // menggambil saldo dari model
        while (rs.next()) {
            System.out.println("\nSaldo UVO anda: Rp. " + df.format(Integer.parseInt(rs.getString("saldo"))) + "\n");
            System.out.println("Tekan 0 untuk kembali ke Menu");
            System.out.println("Tekan 9 untuk keluar aplikasi");
        }
        do {
            System.out.print("->");
            pilih = inputInt.nextInt();

            if (pilih == 0) {
                benar = false;

                // manggil menu dari controller
                uf.menu();
            } else if (pilih == 9) {
                benar = false;

                // manggil keluar dari controller
                uf.keluar();
            } else {
                System.out.println("Pilihan tidak tersedia, ulangi masukan [0/9]: ");
            }
        } while (benar);

    }

    public static void topup() throws SQLException {
        koneksi();
        int pilih, nominal, saldo = 0;
        double point, totalpoint = 0;
        boolean benar = true;

        // mengambil nilai saldo sebelum top up
        String SQL = "Select * from users where id_user='" + id + "'";
        ResultSet rs = stm.executeQuery(SQL);
        while (rs.next()) {
            saldo = rs.getInt("saldo");
            totalpoint = rs.getInt("point");
        }
        do {
            System.out.println("\nTop Up Saldo");
            System.out.println("1. Via Bank");
            System.out.println("2. Via Grobak");
            System.out.println("3. Kembali ke Menu");
            System.out.print("Masukkan pilihan anda: ");
            pilih = inputInt.nextInt();
            switch (pilih) {
                case 1:
                    System.out.print("\nMasukkan Nominal: ");
                    nominal = inputInt.nextInt();
                    point = nominal * 0.01;
                    saldo += nominal;
                    totalpoint += point;
                    benar = false;
                    break;
                case 2:
                    System.out.print("\nMasukkan Nominal: ");
                    nominal = inputInt.nextInt();
                    point = nominal * 0.02;
                    saldo += nominal;
                    totalpoint += point;
                    benar = false;
                    break;
                case 3:
                    uf.menu();
                    benar = false;
                    break;
                default:
                    System.out.println("Pilihan anda salah");
                    break;
            }
        } while (benar);
        // update tb user
        SQL = "Update users set saldo = " + saldo + ", point = " + totalpoint + "where id_user='" + id + "'";
        stm.execute(SQL);
        System.out.println("Saldo berhasil di top up");

        System.out.println("\nTekan 0 untuk kembali ke Menu");
        System.out.println("Tekan 9 untuk keluar aplikasi");
        do {
            System.out.print("->");
            pilih = inputInt.nextInt();

            if (pilih == 0) {
                benar = false;

                // manggil menu dari controller
                uf.menu();
            } else if (pilih == 9) {
                benar = false;

                // manggil keluar dari controller
                uf.keluar();
            } else {
                System.out.println("Pilihan tidak tersedia, ulangi masukan [0/9]: ");
            }
        } while (benar);
    }

    public static void bayarPln() throws SQLException {
        koneksi();
        int pilih, nominal = 0, saldo = 0;
        double point, totalpoint = 0;
        boolean benar = true;

        // mengambil nilai saldo sebelum bayar pln
        String SQL = "Select * from users where id_user='" + id + "'";
        ResultSet rs = stm.executeQuery(SQL);
        while (rs.next()) {
            saldo = rs.getInt("saldo");
            totalpoint = rs.getInt("point");
        }
        do {
            System.out.println("\nBayar PLN");
            System.out.println("1. Rp. 20.000");
            System.out.println("2. Rp. 50.000");
            System.out.println("3. Rp. 100.000");
            System.out.println("4. Rp. 500.000");
            System.out.println("5. Kembali ke Menu");

            System.out.print("Masukkan pilihan: ");
            pilih = inputInt.nextInt();
            switch (pilih) {
                case 1:
                    nominal = 20000;
                    totalpoint += 0;
                    benar = false;
                    break;
                case 2:
                    nominal = 50000;
                    totalpoint += 100;
                    benar = false;
                    break;
                case 3:
                    nominal = 100000;
                    totalpoint += 200;
                    benar = false;
                    break;
                case 4:
                    nominal = 500000;
                    totalpoint += 500;
                    benar = false;
                    break;
                case 5:
                    uf.menu();
                    benar = false;
                    break;
                default:
                    System.out.println("Pilihan anda salah");
                    break;
            }
        } while (benar);

        System.out.println("Masukkan No. ID meter: ");
        int meter = inputInt.nextInt();
        if (saldo < nominal) {
            System.out.println("\nSaldo anda tidak cukup, masukkan pilihan lain");
            uf.bayarPln();
        } else {
            saldo -= nominal;
            // update tb user
            SQL = "Update users set saldo = " + saldo + ", point = " + totalpoint + "where id_user='" + id + "'";
            stm.execute(SQL);
            System.out.println("Pembayaran Berhasil\n");

        }

        System.out.println("Tekan 0 untuk kembali ke Menu");
        System.out.println("Tekan 9 untuk keluar aplikasi");
        do {
            System.out.print("->");
            pilih = inputInt.nextInt();

            if (pilih == 0) {
                benar = false;

                // manggil menu dari controller
                uf.menu();
            } else if (pilih == 9) {
                benar = false;

                // manggil keluar dari controller
                uf.keluar();
            } else {
                System.out.println("Pilihan tidak tersedia, ulangi masukan [0/9]: ");
            }
        } while (benar);
    }

    public static void pulsa() throws SQLException {
        koneksi();
        int pilih, pilih1, nominal = 0, saldo = 0;
        boolean benar = true, benar1 = true;

        // mengambil nilai saldo sebelum bayar pln
        String SQL = "Select * from users where id_user='" + id + "'";
        ResultSet rs = stm.executeQuery(SQL);
        while (rs.next()) {
            saldo = rs.getInt("saldo");
        }
        do {
            System.out.println("\nMenu isi pulsa");
            System.out.println("Pilihan Provider");
            System.out.println("1. Salkomsel");
            System.out.println("2. EngXel");
            System.out.println("3. JavaSat");
            System.out.println("4. Teri");
            System.out.println("5. Kembali ke Menu");
            System.out.print("Masukkan pilihan anda: ");
            pilih = inputInt.nextInt();
            do {
                switch (pilih) {
                    case 1:
                        System.out.println("\nPilihan Nominal");
                        System.out.println("1. Rp. 5000");
                        System.out.println("2. Rp. 10.000");
                        System.out.println("3. Rp. 20.000");
                        System.out.println("4. Rp. 50.000");
                        System.out.println("5. Rp. 100.000");
                        System.out.println("6. Kembali ke Menu");
                        System.out.print("Masukkan pilihan anda: ");
                        pilih1 = inputInt.nextInt();

                        if (pilih1 == 1) {
                            nominal = 7000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 2) {
                            nominal = 12000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 3) {
                            nominal = 22000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 4) {
                            nominal = 52000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 5) {
                            nominal = 102000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 6) {
                            uf.menu();
                            benar = false;
                            benar1 = false;
                        } else {
                            System.out.println("Pilihan tidak tersedia");
                        }
                        break;

                    case 2:
                        System.out.println("\nPilihan Nominal");
                        System.out.println("1. Rp. 5000");
                        System.out.println("2. Rp. 10.000");
                        System.out.println("3. Rp. 20.000");
                        System.out.println("4. Rp. 50.000");
                        System.out.println("5. Rp. 100.000");
                        System.out.println("6. Kembali ke Menu");
                        System.out.print("Masukkan pilihan anda: ");
                        pilih1 = inputInt.nextInt();

                        if (pilih1 == 1) {
                            nominal = 6500;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 2) {
                            nominal = 11500;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 3) {
                            nominal = 21500;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 4) {
                            nominal = 51500;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 5) {
                            nominal = 101500;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 6) {
                            uf.menu();
                            benar = false;
                            benar1 = false;
                        } else {
                            System.out.println("Pilihan tidak tersedia");
                        }
                        break;

                    case 3:
                        System.out.println("\nPilihan Nominal");
                        System.out.println("1. Rp. 5000");
                        System.out.println("2. Rp. 10.000");
                        System.out.println("3. Rp. 20.000");
                        System.out.println("4. Rp. 50.000");
                        System.out.println("5. Rp. 100.000");
                        System.out.println("6. Kembali ke Menu");
                        System.out.print("Masukkan pilihan anda: ");
                        pilih1 = inputInt.nextInt();

                        if (pilih1 == 1) {
                            nominal = 6000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 2) {
                            nominal = 11000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 3) {
                            nominal = 21000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 4) {
                            nominal = 51000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 5) {
                            nominal = 101000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 6) {
                            uf.menu();
                            benar = false;
                            benar1 = false;
                        } else {
                            System.out.println("Pilihan tidak tersedia");
                        }
                        break;
                    case 4:
                        System.out.println("\nPilihan Nominal");
                        System.out.println("1. Rp. 5000");
                        System.out.println("2. Rp. 10.000");
                        System.out.println("3. Rp. 20.000");
                        System.out.println("4. Rp. 50.000");
                        System.out.println("5. Rp. 100.000");
                        System.out.println("6. Kembali ke Menu");
                        System.out.print("Masukkan pilihan anda: ");
                        pilih1 = inputInt.nextInt();

                        if (pilih1 == 1) {
                            nominal = 5000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 2) {
                            nominal = 10000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 3) {
                            nominal = 20000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 4) {
                            nominal = 50000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 5) {
                            nominal = 100000;
                            benar = false;
                            benar1 = false;
                        } else if (pilih1 == 6) {
                            uf.menu();
                            benar = false;
                            benar1 = false;
                        } else {
                            System.out.println("Pilihan tidak tersedia");
                        }
                        break;
                    case 5:
                        uf.menu();
                        benar = false;
                        break;
                    default:
                        System.out.println("Pilihan tidak tersedia");
                        break;
                }
            } while (benar);
        } while (benar1);
        boolean cekNomor = true;
        do {
            System.out.print("Masukkan nomor HP anda: ");
            String nomor = input.next();
            if (nomor.length() < 11 || nomor.length() > 12) {
                System.out.println("Format nomor salah, masukkan 11 atau 12 digit nomor");
            }else{
                cekNomor = false;
            }
        } while (cekNomor);

        if (saldo < nominal) {
            System.out.println("\nSaldo anda tidak cukup, masukkan pilihan lain");
            uf.pulsa();
        } else {
            saldo -= nominal;
            // update tb user
            SQL = "Update users set saldo = " + saldo + "where id_user='" + id + "'";
            stm.execute(SQL);
            System.out.println("Pembelian pulsa berhasil\n");
        }

        System.out.println("Tekan 0 untuk kembali ke Menu");
        System.out.println("Tekan 9 untuk keluar aplikasi");
        do {
            System.out.print("->");
            pilih = inputInt.nextInt();

            if (pilih == 0) {
                benar = false;

                // manggil menu dari controller
                uf.menu();
            } else if (pilih == 9) {
                benar = false;

                // manggil keluar dari controller
                uf.keluar();
            } else {
                System.out.println("Pilihan tidak tersedia, ulangi masukan [0/9]: ");
            }
        } while (benar);
    }

    public static void uvoPoint() throws SQLException {
        int pilih;
        koneksi();
        boolean benar = true, benar1 = true;

        String SQL = "Select * from users where id_user='" + id + "'";
        ResultSet rs = stm.executeQuery(SQL);

        System.out.println("\nUvo Point");
        System.out.println("1. Lihat Uvo Point");
        System.out.println("2. Top Up Uvo");
        System.out.println("3. Kembali ke Menu");
        do {
            System.out.print("Masukkan pilihan anda: ");
            pilih = inputInt.nextInt();
            if (pilih == 1) {
                // menggambil saldo dari model
                while (rs.next()) {
                    System.out.println("\nSaldo UVO Point anda: " + df1.format(Integer.parseInt(rs.getString("point"))) + "\n");

                }

                System.out.println("Tekan 0 untuk kembali ke Menu");
                System.out.println("Tekan 9 untuk keluar aplikasi");
                do {
                    System.out.print("->");
                    pilih = inputInt.nextInt();

                    if (pilih == 0) {
                        benar1 = false;

                        // manggil menu dari controller
                        uf.menu();
                    } else if (pilih == 9) {
                        benar1 = false;

                        // manggil keluar dari controller
                        uf.keluar();
                    } else {
                        System.out.println("Pilihan tidak tersedia, ulangi masukan [0/9]: ");
                    }
                } while (benar1);
                benar = false;
            } else if (pilih == 2) {
                uf.topup();
                benar = false;
            } else if (pilih == 3) {
                uf.menu();
                benar = false;
            } else {
                System.out.println("Pilihan tidak tersedia");
            }
        } while (benar);

    }

    public static void keluar() throws SQLException {
        String pilih;
        boolean benar = true;
        do {
            System.out.print("Apakah kamu yakin mau keluar[y/t] : ");
            pilih = input.next();

            if (pilih.equalsIgnoreCase("y")) {
                benar = false;
                System.exit(0);
            } else if (pilih.equalsIgnoreCase("t")) {
                benar = false;
                uf.menu();
            } else {
                System.out.println("Pilihan anda salah");
                System.out.println("");
            }
        } while (benar);

    }
}
