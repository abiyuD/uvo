/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.UvoModel;
import View.UvoView;
import Controller.UvoFungsi;
import java.sql.SQLException;


/**
 *
 * @author heris
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        
        UvoView uv = new UvoView();
        UvoModel um = new UvoModel();
        UvoKoneksi uk = new UvoKoneksi();
        UvoFungsi uf = new UvoFungsi(um, uk, uv);
        uf.login();
    }
    
}
