package Controller;

import Model.UvoModel;
import View.UvoView;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author abiyu
 */
public class UvoFungsi {
    
    private static UvoModel um;
    private static UvoKoneksi uk;
    private static UvoView uv;

    public UvoFungsi(UvoModel um, UvoKoneksi uk, UvoView uv) {
        this.um = um;
        this.uk = uk;
        this.uv = uv;
    }
    
    
    public static void pulsa() throws SQLException{
        uv.pulsa();
    }
    public static void bayarPln() throws SQLException{
        uv.bayarPln();
    }
    
    public static void lihatSaldo() throws SQLException{
        uv.lihatSaldo();
    }
    
    public static void login() throws SQLException{
        uv.login();
    }
    
    public static void menu() throws SQLException{
        uv.menu();
    }
    
    public static void topup() throws SQLException{
        uv.topup();
    }
    
    public static void uvoPoint() throws SQLException{
        uv.uvoPoint();
    }
    
    public static void keluar() throws SQLException{
        uv.keluar();
    }
    
}
