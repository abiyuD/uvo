package uvo.dao.Koneksi;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.SQLException;

/**
 *
 * @author heris
 */
public class DatabaseKoneksi {
    
    private final SQLServerDataSource sqlSource;

    // construktor
    public DatabaseKoneksi() {
        sqlSource = new SQLServerDataSource();
    }
    
    // open koneksi
    public SQLServerDataSource getSQLServerDataSource(){
        sqlSource.setUser("sa");
        sqlSource.setPassword("12345");
        sqlSource.setDatabaseName("uvo");
        sqlSource.setServerName("localhost");
        sqlSource.setPortNumber(1433);
        
        return sqlSource;
    }
    
    // close koneksi
    public void closeKoneksi(){
        try {
            sqlSource.getConnection().close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
}
