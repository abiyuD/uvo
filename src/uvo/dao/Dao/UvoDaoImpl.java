package uvo.dao.Dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import uvo.dao.Model.Uvo;

/**
 *
 * @author heris
 */
public class UvoDaoImpl implements UvoDao {

    private Connection connect;
    private PreparedStatement getAll;
    private PreparedStatement update;
    private PreparedStatement getData;

    private final String getAllQuery = "select * from users";
    private final String updateQuery = "update users set saldo = ?, point = ? where id_user=?";
    private final String getDataQuery = "select * from user where id_user=?";

    @Override
    public void setKoneksi(Connection connect) throws SQLException {
        this.connect = connect;
        getAll = this.connect.prepareStatement(getAllQuery);
        update = this.connect.prepareStatement(updateQuery);
        getData = this.connect.prepareStatement(getDataQuery);
    }

    @Override
    public List<Uvo> getAll(Uvo uvo) throws SQLException {
        List<Uvo> tr = new ArrayList<>();
        ResultSet rs = getAll.executeQuery();
        while (rs.next()) {
            Uvo u = new Uvo();
            u.setId_user(rs.getString("id_user"));
            u.setPassword(rs.getString("password"));
            u.setSaldo(rs.getString("saldo"));
            u.setPoint(rs.getString("point"));

            // digunakan untuk where
            getAll.setString(1, uvo.getId_user());

            tr.add(u);
        }
        return tr;
    }

    @Override
    public Uvo update(Uvo uvo) throws SQLException {
        update.setString(1, uvo.getSaldo());
        update.setString(2, uvo.getPoint());
        update.setString(3, uvo.getId_user()); // digunakan untuk where

        return uvo;
    }

    @Override
    public void validasiLogin(Uvo uvo) throws SQLException {
        String user, pass;
        ResultSet rs = getAll.executeQuery();
        while (rs.next()) {
            Uvo u = new Uvo();
            u.setId_user(rs.getString("id_user"));
            u.setPassword(rs.getString("password"));

            // digunakan untuk where
            getAll.setString(1, uvo.getId_user());

        }
    }

    @Override
    public void getData(String id_user) throws SQLException {
        ResultSet rs = getData.executeQuery();
        while (rs.next()) {
            Uvo u = new Uvo();
            u.setId_user(rs.getString("id_user"));
            u.setPassword(rs.getString("password"));
            u.setSaldo(rs.getString("saldo"));
            u.setPoint(rs.getString("point"));

            // digunakan untuk where
            getData.setString(1, id_user);
        }
        
    }

}
