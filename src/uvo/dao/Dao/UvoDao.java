package uvo.dao.Dao;

import java.sql.*;
import java.util.List;
import uvo.dao.Model.Uvo;

/**
 *
 * @author heris
 */
public interface UvoDao {

    public void setKoneksi(Connection connect) throws SQLException;

    public List<Uvo> getAll(Uvo uvo) throws SQLException;

    public Uvo update(Uvo uvo) throws SQLException;
    
    public void validasiLogin(Uvo uvo) throws SQLException;
    
    public void getData(String id_user) throws SQLException;

}
