package uvo.dao.Runner;

import java.sql.SQLException;
import uvo.dao.Controller.ControllerUvo;
import uvo.dao.View.UvoView;

/**
 *
 * @author heris
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        UvoView uv = new UvoView();
        
        ControllerUvo cu = new ControllerUvo(uv);
        
        cu.menu();
    }
}
