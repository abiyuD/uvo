package uvo.dao.Controller;

import uvo.dao.View.UvoView;
import java.sql.SQLException;

/**
 *
 * @author heris
 */
public class ControllerUvo {
    
    private final UvoView uv;

    public ControllerUvo(UvoView uv) {
        this.uv = uv;
    }

    
    
    public void menu() throws SQLException{
        uv.menu();
    }
    
}
